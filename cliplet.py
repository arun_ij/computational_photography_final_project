import cv2
import numpy as np
import sys
import os
import commands
import shutil
from pyramid_blending import *

import assignment11

refPt = []
selecting = False
image = np.array((1,1),dtype=np.uint8)

def vizDifference(diff):
    return (((diff - diff.min()) / (diff.max() - diff.min())) * 255).astype( \
        np.uint8)

def runTexture(img_list):
    """ This function administrates the extraction of a video texture from the
      given frames.
    """
    video_volume = assignment11.videoVolume(img_list)
    ssd_diff = assignment11.computeSimilarityMetric(video_volume)
    transition_diff = assignment11.transitionDifference(ssd_diff)
    alpha = 1.0 / transition_diff.shape[0]

    while True:
      #print "Alpha is {}".format(alpha)
      idxs = assignment11.findBiggestLoop(transition_diff, alpha)
      if idxs[1]-idxs[0] < 8:
        alpha *= 1.1
      else:
        break

    diff3 = np.zeros(transition_diff.shape, float)

    for i in range(transition_diff.shape[0]): 
        for j in range(transition_diff.shape[1]): 
            diff3[i,j] = alpha*(i-j) - transition_diff[i,j] 

    return vizDifference(ssd_diff), \
           vizDifference(transition_diff), \
           vizDifference(diff3), \
           assignment11.synthesizeLoop(video_volume, idxs[0]+2, idxs[1]+2), idxs[0]+2,idxs[1]+2

def split_video_cv2(video_path):

    cap = cv2.VideoCapture(video_path)
    
    vid_dir, vid_filename = os.path.split(video_path)
    vid_name, vid_ext = os.path.splitext(vid_filename)
    
    out_dir = os.path.join(vid_dir, vid_name)
    
    if not os.path.exists(out_dir):
      os.mkdir(out_dir)
    
    count = 0
    while cap.grab():
      success, img = cap.retrieve()
      if not success:
        break
      cv2.imwrite(os.path.join(out_dir, 'frame{0:04d}.png'.format(count)), img)
      count += 1

def merge_video_cv2(gif_folder):
    pass

def split_video_ffmpeg(video_path, sourcefolder, framerate):
    if not os.path.exists(sourcefolder):
        os.mkdir(sourcefolder)
    merge_video_command = "ffmpeg -i " + video_path +  " -r "  + str(framerate) + " " + sourcefolder + '/frame%04d.png'
    status, output = commands.getstatusoutput(merge_video_command)
    if status == 0:
        print output
        print "Video successfully split!"
    else:
        print output
    
def cut_video_ffmpege(input_video_file, output_video_file, start_time = 0, video_duration = 3):
    cut_video_comand  = "ffmpeg -i " + input_video_path + " -ss " + str(start_time) + " -t " + str(video_duration) + " -acodec copy -vcodec copy " + output_video_file
    status, output = commands.getstatusoutput(cut_video_command)

def merge_video_ffmpeg(outfolder):
    if os.path.exists('output_video.gif'):
        os.remove('output_video.gif')

    merge_video_command  = "ffmpeg -i " + outfolder + "/frame%04d.png" + " output_video.gif"
    status, output = commands.getstatusoutput(merge_video_command)

class ClipletParams:
    def __init__(self, framerate = 20, input_filename = "input_video.mp4", selected_regions = []):
        self.framerate = framerate
        self.input_filename = input_filename
        self.selected_regions = selected_regions
        self.base_image_id = 0

def generate_first_frame(video_name, image_name):
    #ffmpeg -i input_video.mp4 -vframes 1 -f image2 first_frame.jpg
    if os.path.exists(image_name):
        os.remove(image_name)
    first_frame_command = "ffmpeg -i " + video_name +  " -vframes 1 -f image2 "  + image_name
    status, output = commands.getstatusoutput(first_frame_command)
    
def generate_cliplet(params):

    if not os.path.exists(os.path.join(os.curdir,'videos')):
        os.mkdir(os.path.join(os.curdir,'videos'))
    else:
        shutil.rmtree(os.path.join(os.curdir,'videos'))
        os.mkdir(os.path.join(os.curdir,'videos'))

    if not os.path.exists(os.path.join(os.curdir,'videos', 'source')):
        os.mkdir(os.path.join(os.curdir,'videos', 'source'))
    if not os.path.exists(os.path.join(os.curdir,'videos', 'out')):
        os.mkdir(os.path.join(os.curdir,'videos', 'out'))

    video_path = os.path.abspath(os.path.join(os.curdir,params.input_filename))
    name, ext = os.path.splitext(video_path)
    video_dir = os.path.basename(name)
    if not os.path.exists(video_path):
        print "Video file could not be found at : " + video_path
    
    sourcefolder = os.path.abspath(os.path.join(os.curdir, 'videos', 'source', video_dir))
    if not os.path.exists(sourcefolder):
        os.mkdir(sourcefolder)

    outfolder = os.path.abspath(os.path.join(os.curdir, 'videos', 'out', video_dir))
    if not os.path.exists(outfolder):
        os.mkdir(outfolder)

    print "Splitting video into indidividual images (frame rate = " + str(params.framerate) + ")"
    split_video_ffmpeg(video_path,sourcefolder,params.framerate)

    print 'Searching for video folders in {} folder'.format(sourcefolder)

    # Extensions recognized by opencv
    exts = ['.bmp', '.pbm', '.pgm', '.ppm', '.sr', '.ras', '.jpeg', '.jpg', 
            '.jpe', '.jp2', '.tiff', '.tif', '.png']

  # For every image in the source directory

    img_list = []
    filenames = sorted(os.listdir(sourcefolder))

    start_v=params.selected_regions[0][0]
    end_v=params.selected_regions[0][1]
    start_h=params.selected_regions[0][2]
    end_h=params.selected_regions[0][3]
    #start_x=0
    #end_x=320
    #start_y=0
    #end_y=1920
    img_trunc_list = []
    for filename in filenames:
        name, ext = os.path.splitext(filename)
        if ext in exts:
            img_list.append(cv2.imread(os.path.join(sourcefolder, filename)))

    for i, img in enumerate(img_list):
        img_modified = np.array(img_list[params.base_image_id])
        for region in params.selected_regions:
            start_v = region[0]
            end_v = region[1]
            start_h = region[2]
            end_h = region[3]

            # Create mask
            # Blend image with base image using mask to create modified image
            img_modified[start_v:end_v,start_h:end_h,:] = img[start_v:end_v,start_h:end_h,:]

        img_list[i] = img_modified

    print "Creating video textures for the selected region(s)..."
    print "Extracting video texture frames."
    diff1, diff2, diff3, out_list, start_loop, end_loop = runTexture(img_list)
    print "Start, End = ", start_loop, end_loop
    print "Blending the video textures for the dynamic regions with the static regions..."

   
    base_image = np.array(img_list[params.base_image_id])
    mask_image = np.zeros(base_image.shape, dtype=np.float)
    for region in params.selected_regions:
        start_v = region[0]
        end_v = region[1]
        start_h = region[2]
        end_h = region[3]
        mask_image[start_v:end_v,start_h:end_h,:] = 255.0
    new_list = []
    for i in range(start_loop, end_loop+1):
        img = img_list[i]
        img_modified = pyramid_blender(base_image, img, mask_image)
        new_list.append(img_modified)

    print "writing output to {}".format(outfolder)
    if not os.path.exists(outfolder):
        os.mkdir(outfolder)

    for idx, image in enumerate(new_list):
        cv2.imwrite(os.path.join(outfolder,'frame{0:04d}.png'.format(idx)), image)

    print "Creating gif image..."
    merge_video_ffmpeg(outfolder)
    print "Cliplet generated successfully!"

def select_rectangle(event, x, y, flags, param):
  # grab references to the global variables
  global refPt, selecting, image
 
  # if the left mouse button was clicked, record the starting
  # (x, y) coordinates and indicate that selection is being
  # performed
  if event == cv2.EVENT_LBUTTONDOWN:
    refPt = [(x, y)]
    selecting = True
 
  # check to see if the left mouse button was released
  elif event == cv2.EVENT_LBUTTONUP:
    # record the ending (x, y) coordinates and indicate that
    # the selection operation is finished
    refPt.append((x, y))
    selecting = False
 
    # draw a rectangle around the region of interest
    #dim = (refPt[1][0]-refPt[0][0]+1, refPt[1][1]-refPt[0][1]+1, 3)
    #image  = np.array(dim)
    cv2.rectangle(image, refPt[0], refPt[1], (0, 255, 0), 2)
    cv2.imshow("image", image)

def read_selected_region(input_image):
  # load the image, clone it, and setup the mouse callback function
  global image, refPt, selecting
  image = cv2.imread(input_image)
  clone = image.copy()
  cv2.namedWindow("image")
  cv2.setMouseCallback("image", select_rectangle)
   
  # keep looping until the 'q' key is pressed
  while True:
    # display the image and wait for a keypress
    cv2.imshow("image", image)
    key = cv2.waitKey(1) & 0xFF
   
    # if the 'r' key is pressed, reset the selection region
    if key == ord("r"):
      image = clone.copy()
      refPt = []
      print "Selected region reset!"
   
    # if the 'c' key is pressed, break from the loop
    elif key == ord("s"):
      print "Selected region saved! Returning to main menu."
      break

  cv2.destroyAllWindows()
  return refPt

def menu(params):
    number = 0

    while True:
        mapping = {}

        number += 1
        mapping['video_file'] = number
        print
        print str(number) + ". Specify the video file (current file = " + params.input_filename + ")\n"

        number += 1
        mapping['framerate'] = number
        print str(number) + ". Specify video split frame rate (current framerate = " + str(params.framerate) + ")\n"

        #number += 1
        #mapping['alpha'] = number
        #print str(number) + ". Specify alpha (current alpha = " + str(params.alpha) + ")\n"

        number += 1
        mapping['create dynamic text'] = number
        print str(number) + ". Specify new dynamic region manually"
        print "        Current regions:"
        for i,region in enumerate(params.selected_regions):
            print "        Region {}: [{}, {}, {}, {}] ".format(i+1, region[0], region[1], region[2], region[3])
        print

        number += 1
        mapping['create dynamic graphic'] = number
        print str(number) + ". Specify new dynamic region graphically\n"

        number += 1
        mapping['delete dynamic'] = number
        print str(number) + ". Delete an existing dynamic region\n"

        number += 1
        mapping['generate cliplet'] = number
        print str(number) + ". Generate cliplet\n"

        number += 1
        mapping['exit'] = number
        print str(number) + ". Exit\n"

        choice = input("Enter your choice: ")
        if not (choice in range(1,number+1)):
            print "Invaild choice. Choice should be a number form 1 to " + str(number)

        if choice == mapping['video_file']:
            input_filename = raw_input("Enter the video filename: ")
            params.input_filename = input_filename
            #generate_first_frame(input_filename, 'first_frame.jpg')
        elif choice == mapping['framerate']:
            framerate = input("Enter the video split framerate: ")
            params.framerate = framerate
        #elif choice == mapping['alpha']:
        #    alpha = input("Enter the alpha: ")
        #    params.alpha = alpha
        elif choice == mapping['create dynamic text']:
            print "Enter the limits of the dynamic region: "
            start_v, end_v = map(int,raw_input("Enter the vertical limits (separated by spaces): ").split())
            start_h, end_h = map(int,raw_input("Enter the horizontal limits (separated by spaces): ").split())
            params.selected_regions.append((int(start_v), int(end_v), int(start_h), int(end_h)))
        elif choice == mapping['create dynamic graphic']:
            print
            print "INSTRUCTIONS FOR SELECTING A DYNAMIC REGION"
            print "************************************************"
            print "Step 1: Press the \"Enter\" key to launch the selection window."
            print "Step 2: Select a rectangular region using the mouse (highlighted in green)"
            print "Step 3 (optional): To reset a selected region press the \"r\" key"
            print "Step 4: To save a selected region, press the \"s\" key" 
            key = raw_input("Press the \"Enter\" key to launch the selection window")
            
            if os.path.exists(params.input_filename):
                generate_first_frame(params.input_filename, 'first_frame.jpg')
            else:
                print "Specify the input video by selecting menu option " + str(mapping['video_file']) + "."
                continue
            rectangle = read_selected_region('first_frame.jpg')
            cv2.destroyAllWindows()
            start_v = min(rectangle[0][1], rectangle[1][1])
            end_v = max(rectangle[0][1], rectangle[1][1])
            start_h = min(rectangle[0][0], rectangle[1][0])
            end_h = max(rectangle[0][0], rectangle[1][0])
            params.selected_regions.append((int(start_v), int(end_v), int(start_h), int(end_h)))
        elif choice == mapping['delete dynamic']:
            if len(params.selected_regions) == 0:
                print "There are no regions to delete!"
                print "Please create a dynamic region either graphically (menu option="+ \
                  str(mapping['create dynamic text']) + " or " + str(mapping['create dynamic graphic']) + ")"
                print
                number = 0
                continue
            print "        Current regions:"
            for i,region in enumerate(params.selected_regions):
                print "        Region {}: [{}, {}, {}, {}] ".format(i+1, region[0], region[1], region[2], region[3])
            region_id = input("Enter the dynamic region to be deleted: ")

            if region_id in range(1,len(params.selected_regions)+1):
              del params.selected_regions[region_id-1]
            else:
                print "The id of the region to be deleted should be in the range 1-" + str(len(params.selected_regions))
                print
                number = 0
                continue
        elif choice == mapping['generate cliplet']:
            generate_cliplet(params)
        elif choice == mapping['exit']:
            print "Exiting program!"
            break
        else:
            number = 0
            continue

        number = 0

if __name__ == "__main__":
    params = ClipletParams()
    menu(params)

